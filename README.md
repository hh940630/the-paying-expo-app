# 더펭 - 모임의 더치페이를 더 빠르고 쉽게!

> 다양한 모임의 더치페이를 즉시 결제 할 수 있어요! 계좌를 등록하고 모임 그룹을 만드세요. 모임에서 사용된 사용내역을 다 같이 한번에 확인하고 총무의 결제의 요청 시 
비밀번호만 입력하면 더치페이완료!!


### 실행하기
 ** 모바일 기기에 미리 `expo` 앱을 설치해줍니다.
 
1. terminal에서 프로젝트를 위치시키고 싶은 폴더 위치에서 `git clone https://gitlab.com/hh940630/the-paying-expo-app.git` 실행시켜줍니다.
2. VSCode에서 프로젝트 root 폴더(the-paying-expo-app)를 열어줍니다.
3. root 폴더에서 `npm install`
4. root 폴더에서 `npm start` 
5. 브라우저에 Expo DevTools가 열립니다.
6. 좌측하단에서 `connection = LAN` 으로 선택합니다.
7. iOS의 경우 일반 카메라를 켜서 QRCode를 읽어줍니다. Android의 경우 QRCode를 읽을 수 있는 카메라(ex. 네이버 QRCode 카메라)를 켜서 앱을 실행시켜줍니다.
8. `실행완료`