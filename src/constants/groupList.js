export const groupList = [
  {
    no: 1,
    groupName: '[KISA] Fintech 프로젝트 모임',
    groupSchedule: '매주 토요일 12:00 ~ 15:00',
    groupStatus: 1,
  },
  {
    no: 2,
    groupName: '모의 면접 모임',
    groupSchedule: '매주 화요일 10:00 ~ 12:00',
    groupStatus: 1,
  },
  {
    no: 3,
    groupName: '서버 스터디 모임',
    groupSchedule: '매주 목요일 13:00 ~ 18:00',
    groupStatus: 1,
  },
  {
    no: 4,
    groupName: '대학교 컴퓨터공학과 동기 모임',
    groupSchedule: '매달 마지막주 금 19:00 ~ ',
    groupStatus: 1,
  },
  {
    no: 5,
    groupName: '강남역 독서모임',
    groupSchedule: '매주 수요일 18:00 ~ 20:00',
    groupStatus: 0,
  },
  {
    no: 6,
    groupName: '석촌호수 크로스핏 모임',
    groupSchedule: '매주 월/수/토요일 19:00 ~ 20:30',
    groupStatus: 0,
  },
  {
    no: 7,
    groupName: '알고리즘 스터디 모임',
    groupSchedule: '매주 월/수/토요일 19:00 ~ 20:30',
    groupStatus: 0,
  },
  {
    no: 8,
    groupName: '한강 자전거 모임',
    groupSchedule: '매주 토요일 17:00 ~ 19:00',
    groupStatus: 0,
  },
];
