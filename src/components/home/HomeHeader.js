import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import TextCom from '../../components/common/TextCom';

class HomeHeader extends Component {
    render() {
        return (
            <View style={styles.wrapper}>
                <View style={styles.userImageWrapper}>
                    <View style={styles.userImage}>
                        <Icon name="ios-person" style={styles.icon} />
                    </View>
                </View>
                <View style={styles.userNameWrapper}>
                    <TextCom style={styles.helloText}>안녕하세요!</TextCom>
                    <TextCom style={styles.userNameText}>nickname 님</TextCom>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: '6%',
        paddingRight: '6%',
        borderBottomWidth: 6,
        borderBottomColor: '#F1F2F3',
    },

    userImageWrapper: { flex: 1 },
    userImage: {
        backgroundColor: '#F1F2F3',
        height: '70%',
        aspectRatio: 1,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon: { fontSize: 60, color: '#BBB' },

    userNameWrapper: {
        flex: 2,
        justifyContent: 'space-around',
        height: '60%',
        marginLeft: '5%',
    },
    helloText: { fontSize: 26, color: '#333' },
    userNameText: { fontSize: 26, color: '#333', fontWeight: 'bold' },
});

export default HomeHeader;
