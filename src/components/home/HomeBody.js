import { FlatList, StyleSheet, View } from 'react-native';
import React, { Component } from 'react';

import GroupItem from './GroupItem';
import { groupList } from '../../constants/groupList';

class HomeBody extends Component {
    render() {
        return (
            <View style={styles.wrapper}>
                <FlatList
                    keyExtractor={(item) => item.toString()}
                    data={groupList}
                    refreshing={true}
                    renderItem={({ item }) => (
                        <GroupItem
                            no={item.no}
                            groupName={item.groupName}
                            groupSchedule={item.groupSchedule}
                            groupStatus={item.groupStatus}
                        />
                    )}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: { flex: 4, backgroundColor: 'white' },
});

export default HomeBody;
