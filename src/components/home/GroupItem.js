import {
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';

import React from 'react';
import TextCom from '../../components/common/TextCom';

const GroupItem = ({ ...props }) => {
    return (
        <TouchableOpacity style={styles.wrapper} activeOpacity={0.7}>
            <View
                style={[
                    styles.boxWrapper,
                    Platform.OS === 'ios' ? styles.iosShadow : styles.andShadow,
                ]}
            >
                <View style={styles.groupImage} />
                <View style={styles.groupInfoWrapper}>
                    <Text style={styles.groupNumText}>No. {props.no}</Text>
                    <Text style={styles.groupNameText}>{props.groupName}</Text>
                    <Text style={styles.groupInfoText}>
                        {props.groupSchedule}
                    </Text>
                </View>
                <View
                    style={[
                        styles.groupStatus,
                        {
                            backgroundColor:
                                props.groupStatus === 1 ? '#1DDB16' : '#A6A6A6',
                        },
                    ]}
                >
                    <TextCom style={styles.groupStatusText}>
                        {props.groupStatus === 1 ? '활동중' : '활동\n종료'}
                    </TextCom>
                </View>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 4,
        marginBottom: 4,
    },
    boxWrapper: {
        height: '80%',
        width: '88%',
        backgroundColor: '#F1F2F3',
        borderRadius: 8,
        flexDirection: 'row',
        alignItems: 'center',
    },
    iosShadow: {
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    },
    andShadow: { elevation: 5 },

    groupImage: {
        marginLeft: '5%',
        marginRight: '5%',
        width: 54,
        aspectRatio: 1,
        backgroundColor: '#BBB',
        borderRadius: 6,
    },
    groupInfoWrapper: { flex: 4, height: 54, justifyContent: 'space-between' },
    groupNumText: { fontSize: 8, color: '#666' },
    groupNameText: { fontSize: 13, color: '#333' },
    groupInfoText: { fontSize: 12, color: '#444' },
    groupStatus: {
        marginLeft: '5%',
        marginRight: '5%',
        width: 54,
        aspectRatio: 1,
        borderRadius: 6,
        justifyContent: 'center',
        alignItems: 'center',
    },
    groupStatusText: { color: 'white', fontSize: 12, fontWeight: 'bold' },
});

export default GroupItem;
