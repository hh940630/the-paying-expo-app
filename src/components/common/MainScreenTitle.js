import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

import { IdentityColor } from '../../../assets/theme/color';
import TextCom from './TextCom';

class ScreenTitle extends Component {
    _renderToday = () => {
        let today = new Date().toDateString();
        return today;
    };

    render() {
        return (
            <View style={styles.wrapper}>
                <View style={styles.logo}>
                    <TextCom style={styles.logoText}>더펭</TextCom>
                </View>
                <View style={styles.dateWrapper}>
                    <TextCom style={styles.dateText}>
                        {this._renderToday()}
                    </TextCom>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 13,
        paddingBottom: 13,
        paddingLeft: '6%',
        paddingRight: '6%',
    },
    logo: { width: 100, height: 28 },
    logoText: { fontSize: 24, fontWeight: 'bold', color: IdentityColor },
    dateWrapper: { justifyContent: 'center', alignItems: 'flex-end' },
    dateText: { fontSize: 12, color: '#333' },
});

export default ScreenTitle;
