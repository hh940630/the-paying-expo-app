import {StyleSheet, TouchableOpacity, View} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import React from 'react';
import TextCom from './TextCom';

const ScreenTitle = ({...props}) => {
  return (
    <View style={[styles.wrapper, props.andStyle]}>
      {props.arrowVisible ? (
        <TouchableOpacity
          onPress={props.goto}
          style={styles.arrowWrapper}
          hitSlop={{top: 9, right: 9, bottom: 9, left: 9}}>
          <Icon
            name="ios-arrow-round-back"
            size={30}
            style={{color: props.backBtnColor}}
          />
        </TouchableOpacity>
      ) : (
        <View style={styles.titleBlank} />
      )}
      <TextCom style={[styles.title, {color: props.fontColor}]}>
        {props.title}
      </TextCom>
      <View style={styles.titleBlank} />
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    paddingTop: 13,
    paddingBottom: 13,
    paddingLeft: 19,
    paddingRight: 19,
  },
  arrowWrapper: {alignItems: 'flex-start', width: '15%'},
  arrow: {fontSize: 26, padding: 4, width: '100%'},
  titleBlank: {width: '15%'},
  title: {fontSize: 16, alignItems: 'center', fontWeight: '400'},
});

export default ScreenTitle;
