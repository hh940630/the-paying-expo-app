import {SafeAreaView, StyleSheet} from 'react-native';

import React from 'react';

const ScreenWrapper = ({children, ...props}) => {
  return (
    <SafeAreaView style={[styles.wrapper, props.andStyle]}>
      {children}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  wrapper: {flex: 1},
});

export default ScreenWrapper;
