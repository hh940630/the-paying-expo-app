import {Platform, Text} from 'react-native';

import React from 'react';

const defaultStyle = Platform.select({
  ios: {
    fontFamily: 'AppleSDGothicNeo-Regular',
    fontWeight: '300',
  },
  android: {
    fontFamily: 'sans-serif',
    includeFontPadding: false,
  },
});

const TextCom = ({children, style, ...props}) => {
  return (
    <Text {...props} allowFontScaling={false} style={[defaultStyle, style]}>
      {children}
    </Text>
  );
};

export default TextCom;
