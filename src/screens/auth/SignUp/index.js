import React, { Component } from 'react';
import { StyleSheet, TextInput, TouchableOpacity, View } from 'react-native';

import { IdentityColor } from '../../../../assets/theme/color';
import ScreenTitle from '../../../components/common/ScreenTitle';
import ScreenWrapper from '../../../components/common/ScreenWrapper';
import TextCom from '../../../components/common/TextCom';

class SignUp extends Component {
    render() {
        return (
            <ScreenWrapper andStyle={styles.wrapper}>
                <ScreenTitle title="회원가입" />
                <View style={styles.loginWrapper}>
                    <View style={styles.inputBox}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="닉네임을 입력해주세요."
                        />
                    </View>
                    <View style={[styles.inputBox, { marginBottom: '4%' }]}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="이메일을 입력해주세요."
                        />
                    </View>

                    <TouchableOpacity
                        style={[
                            styles.buttonWrapper,
                            { backgroundColor: '#666' },
                        ]}
                        activeOpacity={0.8}
                    >
                        <TextCom style={styles.buttonText}>
                            인증하기(필수)
                        </TextCom>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.buttonWrapper}
                        activeOpacity={0.8}
                    >
                        <TextCom style={styles.buttonText}>회원가입</TextCom>
                    </TouchableOpacity>
                    <View style={styles.divider} />
                    <TouchableOpacity
                        style={[styles.buttonWrapper, styles.signupButton]}
                        onPress={() => this.props.navigation.navigate('Login')}
                        activeOpacity={0.8}
                    >
                        <TextCom style={[styles.buttonText, styles.signupText]}>
                            로그인
                        </TextCom>
                    </TouchableOpacity>
                </View>
            </ScreenWrapper>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: { backgroundColor: 'white' },
    loginWrapper: {
        flex: 1,
        alignItems: 'center',
        paddingTop: '4%',
        paddingBottom: '4%',
        borderTopColor: '#F1F1F1',
        borderTopWidth: 3,
    },
    inputBox: {
        width: '88%',
        height: 48,
        alignItems: 'flex-start',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#BBB',
        borderRadius: 4,
        marginTop: '4%',
        marginBottom: '2%',
    },
    textInput: { paddingLeft: '4%' },

    buttonWrapper: {
        width: '88%',
        height: 48,
        backgroundColor: IdentityColor,
        borderColor: 'white',
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '2%',
        marginBottom: '2%',
    },
    buttonText: { color: 'white', fontSize: 18, fontWeight: '600' },
    findIdWrapper: {
        marginTop: '2%',
        marginBottom: '4%',
    },
    findIdText: { color: '#666' },
    divider: {
        width: '88%',
        height: 1,
        borderWidth: 0.5,
        borderColor: '#BBB',
        borderStyle: 'dashed',
    },
    signupButton: {
        backgroundColor: 'white',
        borderColor: IdentityColor,
        borderWidth: 1,
    },
    signupText: { color: IdentityColor },
});

export default SignUp;
