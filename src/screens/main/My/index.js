import React, { Component } from 'react';
import { StyleSheet, TextInput, TouchableOpacity, View } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import { IdentityColor } from '../../../../assets/theme/color';
import ScreenTitle from '../../../components/common/ScreenTitle';
import ScreenWrapper from '../../../components/common/ScreenWrapper';
import TextCom from '../../../components/common/TextCom';

class My extends Component {
    render() {
        return (
            <ScreenWrapper andStyle={styles.wrapper}>
                <ScreenTitle title="My Page" />
            </ScreenWrapper>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: { backgroundColor: 'white' },
});

export default My;
