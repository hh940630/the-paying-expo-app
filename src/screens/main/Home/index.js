import React, { Component } from 'react';
import { StyleSheet, TextInput, TouchableOpacity, View } from 'react-native';

import HomeBody from '../../../components/home/HomeBody';
import HomeHeader from '../../../components/home/HomeHeader';
import MainScreenTitle from '../../../components/common/MainScreenTitle';
import ScreenWrapper from '../../../components/common/ScreenWrapper';

class Home extends Component {
    render() {
        return (
            <ScreenWrapper andStyle={styles.wrapper}>
                <MainScreenTitle />
                <HomeHeader />
                <HomeBody />
            </ScreenWrapper>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: { backgroundColor: 'white' },
    header: { flex: 1, flexDirection: 'row', backgroundColor: 'red' },
    body: {},
});

export default Home;
