import { IdentityColor } from '../../../../assets/theme/color';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    wrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: IdentityColor,
    },
    titleText: {
        fontSize: 34,
        color: 'white',
        fontWeight: 'bold',
    },
    desText: {
        fontSize: 12,
        color: '#F2F2F2',
        textAlign: 'center',
        marginTop: 16,
    },
});
