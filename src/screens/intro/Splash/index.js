import React, { Component } from 'react';

import TextCom from '../../../components/common/TextCom';
import { View } from 'react-native';
import styles from './style';

export default class Splash extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.navigate('Login');
        }, 1500);
    }
    render() {
        return (
            <View style={styles.wrapper}>
                <TextCom style={styles.titleText}>더펭</TextCom>
                <TextCom style={styles.desText}>
                    모임의 더치페이를 더 빠르고 쉽게!모임의 더치페이를 더 빠르고
                    쉽게!
                </TextCom>
            </View>
        );
    }
}
