import Home from '../screens/main/Home';
import Icon from 'react-native-vector-icons/Ionicons';
import { IdentityColor } from '../../assets/theme/color';
import My from '../screens/main/My';
import React from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

const MainNavigation = createMaterialTopTabNavigator(
    {
        HomeTab: {
            screen: Home,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <Icon
                        name="ios-home"
                        style={{ color: tintColor, fontSize: 28 }}
                    />
                ),
            },
        },
        Search: {
            screen: My,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <Icon
                        name="ios-person"
                        style={{ color: tintColor, fontSize: 28 }}
                    />
                ),
            },
        },
    },
    {
        animationEnabled: true,
        swipeEnabled: true,
        tabBarPosition: 'bottom',
        tabBarOptions: {
            style: {
                height: 50,
                bottom: 30,
                backgroundColor: '#FFFFFF',
            },
            iconStyle: { height: 30 },
            indicatorStyle: {
                backgroundColor: 'white',
                borderBottomColor: IdentityColor,
                borderBottomWidth: 2,
            },
            activeTintColor: IdentityColor,
            inactiveTintColor: '#888',
            upperCaseLabel: true,
            showLabel: true,
            showIcon: true,
        },
    }
);
// import {createBottomTabNavigator} from 'react-navigation-tabs';

// const MainNavigation = createBottomTabNavigator(
//   {
//     Home: {
//       screen: Home,
//       navigationOptions: {
//         tabBarIcon: ({focused}) => (
//           <Icon
//             name="ios-home"
//             style={{
//               fontSize: 32,
//               color: focused ? IdentityColor : '#BBB',
//             }}
//           />
//         ),
//       },
//     },
//     My: {
//       screen: My,
//       navigationOptions: {
//         tabBarIcon: ({focused}) => (
//           <Icon
//             name="ios-person"
//             style={{
//               fontSize: 32,
//               color: focused ? IdentityColor : '#BBB',
//             }}
//           />
//         ),
//       },
//     },
//   },
//   {
//     tabBarOptions: {
//       animationEnabled: true,
//       swipeEnabled: true,
//       activeTintColor: IdentityColor, // identity color
//       inactiveTintColor: '#FFFFFF',
//       style: {
//         backgroundColor: 'white',
//         borderTopWidth: 0,
//         height: 32,
//         marginTop: 10,
//         marginBottom: 10,
//       },
//       showLabel: false,
//       showIcon: true,
//     },
//   },
// );

export default MainNavigation;
