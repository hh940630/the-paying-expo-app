import 'react-native-gesture-handler';

import LoginScreen from '../screens/auth/Login';
import SignUpScreen from '../screens/auth/SignUp';
import { createStackNavigator } from 'react-navigation-stack';

const LoginNavigation = createStackNavigator(
    {
        Login: {
            screen: LoginScreen,
        },
        SignUp: {
            screen: SignUpScreen,
        },
    },
    {
        initialRouteName: 'Login',
        defaultNavigationOptions: {
            headerShown: false,
        },
    }
);

export default LoginNavigation;
