import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import LoginNavigation from './LoginNavigation';
import MainNavigation from './MainNavigation';
import SplashScreen from '../screens/intro/Splash';

export default createAppContainer(
    createSwitchNavigator(
        {
            Splash: SplashScreen,
            Login: LoginNavigation,
            Main: MainNavigation,
        },
        {
            initialRouteName: 'Splash',
        }
    )
);
